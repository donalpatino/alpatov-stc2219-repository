public class Main {
    public static void main(String[] args) {
        int[] numbers = {1,2,3,4,5,6,7,8,9};

        System.out.println("Сумма всех чисел в массиве");
        System.out.println(MyMath.sum(numbers));

        System.out.println("Вычитание всех чисел из первого в массиве");
        System.out.println(MyMath.subtraction(numbers[0], numbers));

        System.out.println("Вычитание всех чисел из большего в массиве");
        System.out.println(MyMath.subtraction(MyMath.getMax(numbers), numbers));

        System.out.println("Перемножаем все числа в массиве");
        System.out.println(MyMath.multiplication(numbers));

        System.out.println("Делим первое число на все последующие числа в массиве");
        System.out.println(MyMath.divide(numbers));
    }
}
