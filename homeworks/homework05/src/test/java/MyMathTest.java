import org.junit.jupiter.api.Test;

class MyMathTest {
    int[] numbers = {1,2,3,4,5,6,7,8,9};
    @Test
    void sum() {
        assert MyMath.sum(numbers) == 45;
    }

    @Test
    void subtraction() {
        assert MyMath.subtraction(numbers[0], numbers) == -43;
    }

    @Test
    void multiplication() {
        assert MyMath.multiplication(numbers) == 362880;
    }

    @Test
    void divide() {
        assert MyMath.divide(numbers) == 1;
    }

    @Test
    void subtractionFromBiggerNumber() {
        assert MyMath.subtraction(MyMath.getMax(numbers),numbers) == -35;
    }
}