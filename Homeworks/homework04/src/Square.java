import java.util.Random;

public class Square extends Rectangle implements Moveable {

    @Override
    public int getPerimeter() {
        return side1 * side1;
    }

    public Square(int side) {
        super(side, side);
    }

    public void move() {
        Random random = new Random();
        this.x = random.nextInt(10);
        this.y = random.nextInt(10);
    }

    @Override
    public String toString() {
        return "Square";
    }
}
