public class Rectangle extends Figure {
    int side1;
    int side2;

    public Rectangle(int side1, int side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public int getPerimeter() {
        return side1 * side2;
    }

    @Override
    public String toString() {
        return "Rectangle";
    }
}
