import java.util.Arrays;
import java.util.Random;

public class Main {

    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(1000) + 1;
        }
        System.out.println("Оригинальный массив");
        System.out.println(Arrays.toString(array));

        int[] newarray01 = Sequence.filter(array, number -> number % 2 == 0);

        System.out.println("Массив четных чисел");
        System.out.println(Arrays.toString(newarray01));

        int[] newarray02 = Sequence.filter(array, number -> Util.sumOfDigit(number) % 2 == 0);

        System.out.println("Массив чисел сумма цифр которых четная");
        System.out.println(Arrays.toString(newarray02));

        int[] newarray03 = Sequence.filter(array, number -> Util.isAllDigitEven(number));
        System.out.println("Массив чисел все цифры которых четные");
        System.out.println(Arrays.toString(newarray03));

        int[] newarray04 = Sequence.filter(array, number -> number == Util.reverse(number));
        System.out.println("Массив чисел все числа которого палиндромы");
        System.out.println(Arrays.toString(newarray04));

    }

}



