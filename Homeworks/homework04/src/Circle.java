import java.util.Random;

public class Circle extends Ellipse implements Moveable {
    public Circle(int radius) {
        super(radius, radius);
    }

    @Override
    public int getPerimeter() {
        return (int) (2 * Math.PI * maxRadius);
    }

    public void move() {
        Random random = new Random();
        this.x = random.nextInt(10);
        this.y = random.nextInt(10);
    }

    @Override
    public String toString() {
        return "Circle";
    }
}
