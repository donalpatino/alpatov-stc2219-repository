import java.util.Arrays;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Figure ellipse = new Ellipse(4, 2);
        Figure rectangle = new Rectangle(5, 10);
        Figure square = new Square(10);
        Figure circle = new Circle(2);

        Figure[] figures = {ellipse, rectangle, square, circle};
        Stream<Figure> stream = Arrays.stream(figures);
        stream.forEach(x -> {
            if (x instanceof Moveable) {
                ((Moveable) x).move();
                System.out.println("Фигура " + x + " переместилась успешно на позицию " + x.getX() + " " + x.getY());
            }
            System.out.println("Периметр фигуры " + x + " = " + x.getPerimeter());
        });
    }
}


