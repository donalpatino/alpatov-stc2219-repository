public class Ellipse extends Figure {
    int maxRadius;
    int minRadius;

    public Ellipse(int maxRadius, int minRadius) {
        this.maxRadius = maxRadius;
        this.minRadius = minRadius;
    }

    @Override
    public int getPerimeter() {
        int perimeter;
        perimeter = (int) (4 * (Math.PI * maxRadius * minRadius + (maxRadius - minRadius)) / (maxRadius + minRadius));
        return perimeter;
    }

    @Override
    public String toString() {
        return "Ellipse";
    }
}
