public class Util {
    public static int sumOfDigit(int number) {
        int sum = 0;
        while (number != 0) {
            //Суммирование цифр числа
            sum += (number % 10);
            number /= 10;
        }
        return sum;
    }

    public static int reverse(int number) {
        int reversed = 0;
        // базовый вариант
        if (number == 0) {
            return number;
        }
        while (number != 0) {

            int digit = number % 10;
            reversed = reversed * 10 + digit;
            number /= 10;
        }
        return reversed;
    }

    public static boolean isAllDigitEven(int number) {
        int sum = 0;
        while (number != 0) {
            if (number % 10 % 2 == 1) return false;
            number /= 10;
        }
        return true;
    }
}
