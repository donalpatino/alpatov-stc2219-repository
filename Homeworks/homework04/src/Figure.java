public abstract class Figure {
    int x;
    int y;

    public abstract int getPerimeter();

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
