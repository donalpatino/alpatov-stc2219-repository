import java.util.Arrays;

public class MyMath {

    static int sum(int... numbers) {
        return Arrays.stream(numbers).sum();
    }

    static int subtraction(int minuend, int ... numbers) {
        for (int i = 1; i < numbers.length; i++) {
            minuend = minuend - numbers[i];
        }
        return minuend;
    }

    static int multiplication(int... numbers) {
        return Arrays.stream(numbers).reduce(1, (a, b) -> a * b);
    }

    static int divide(int... numbers) {
        int dividend = numbers[0];
        for (int i = 1; i < numbers.length; i++) {
            if (dividend <= numbers[i] || numbers[i] <= 0 ) return dividend;
            dividend = dividend/numbers[i];
        }
        return dividend;
    }

    public static int getMax(int[] inputArray) {
        int maxValue = inputArray[0];
        for (int i = 1; i < inputArray.length; i++) {
            if (inputArray[i] > maxValue) {
                maxValue = inputArray[i];
            }
        }
        return maxValue;
    }
}
