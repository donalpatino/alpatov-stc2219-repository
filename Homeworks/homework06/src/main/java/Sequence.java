import java.util.Arrays;

public class Sequence {
    public static int[] filter(int[] array, ByCondition condition) {
        int[] tmparray = new int[array.length];
        int j = 0;
        for (int i = 0; i < array.length; i++) {
            if (checkNumber(array[i], condition)) {
                tmparray[j++] = array[i];
            }
        }
        return Arrays.copyOfRange(tmparray, 0, j);
    }

    public static boolean checkNumber(int number, ByCondition byCondition) {
        return byCondition.isOk(number);
    }

}
